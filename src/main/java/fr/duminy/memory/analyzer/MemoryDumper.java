package fr.duminy.memory.analyzer;

import org.slf4j.Logger;

import java.io.IOException;
import java.nio.file.Path;

import static java.lang.management.ManagementFactory.getPlatformMXBean;
import static org.slf4j.LoggerFactory.getLogger;

public class MemoryDumper {
    static final String HPROF_EXTENSION = ".hprof";
    private static final Logger LOGGER = getLogger(MemoryDumper.class);

    public Path dumpHeap(Path outputDirectory, String baseFileName) throws IOException {
        Path hProfFile = getHProfFile(outputDirectory, baseFileName);
        dumpHeap(hProfFile);
        return hProfFile;
    }

    private Path getHProfFile(Path outputDirectory, String fileName) {
        if (!fileName.endsWith(HPROF_EXTENSION)) {
            fileName += HPROF_EXTENSION;
        }
        return outputDirectory.resolve(fileName);
    }

    private void dumpHeap(Path filePath) throws IOException {
        @SuppressWarnings("java:S1191")
        com.sun.management.HotSpotDiagnosticMXBean mxBean = getPlatformMXBean(com.sun.management.HotSpotDiagnosticMXBean.class);

        String outputFile = filePath.toAbsolutePath().toString();
        mxBean.dumpHeap(outputFile, false);
        LOGGER.info("dumpHeap(outputFile={})", outputFile);
    }
}
