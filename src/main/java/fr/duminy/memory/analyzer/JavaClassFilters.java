package fr.duminy.memory.analyzer;

public class JavaClassFilters {
    public static ClassNameStartingWith classNameStartingWith(String begin) {
        return new ClassNameStartingWith(begin);
    }

    public static ClassNameEquals classNameEquals(String className) {
        return new ClassNameEquals(className);
    }

    private JavaClassFilters() {
        // utility class
    }
}
