package fr.duminy.memory.analyzer;

import fr.duminy.memory.analyzer.MemoryDumpAnalyzer.Statistics;
import org.slf4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static java.lang.String.join;
import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.util.Locale.FRANCE;
import static org.slf4j.LoggerFactory.getLogger;

class StatisticsWriter {
    private static final Logger LOGGER = getLogger(StatisticsWriter.class);
    private static final Locale LOCALE = FRANCE;

    void write(Map<String, Statistics> statisticsByName, Path statisticsFile) {
        List<String> lines = new ArrayList<>();
        lines.add("name,countBefore,countAfter,deltaCount,deltaPercent");
        statisticsByName.forEach((name, statistics) ->
                lines.add(join(",",
                        name,
                        format(statistics.countBefore.get()),
                        format(statistics.countAfter.get()),
                        format(statistics.deltaCount),
                        format(statistics.deltaPercent))));
        try {
            Files.write(statisticsFile, lines, CREATE, APPEND);
        } catch (IOException e) {
            LOGGER.error("Error in writeStatistics for child {}. cause:{}", e.getMessage(), e);
        }
    }

    private static String format(double value) {
        return String.format(LOCALE, "\"%10.0f\"", value);
    }
}
