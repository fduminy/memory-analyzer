package fr.duminy.memory.analyzer;

import fr.duminy.memory.analyzer.HProfFileReader.HeapWrapper;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

import static java.util.Arrays.asList;
import static java.util.stream.StreamSupport.stream;

public class MemoryDumpAnalyzer {
    private final HProfFileReader hProfFileReader;
    private final StatisticsWriter statisticsWriter;
    private final List<JavaClassFilter> javaClassFilters;

    public MemoryDumpAnalyzer(HProfFileReader hProfFileReader, StatisticsWriter statisticsWriter, JavaClassFilter... javaClassFilters) {
        this.hProfFileReader = hProfFileReader;
        this.statisticsWriter = statisticsWriter;
        this.javaClassFilters = asList(javaClassFilters);
    }

    public void analyze(Path hProfFileBefore, Path hProfFileAfter, Path statisticsFile) {
        Map<String, Statistics> statisticsByName = computeStatistics(hProfFileBefore, hProfFileAfter);
        statisticsWriter.write(statisticsByName, statisticsFile);
    }

    private Map<String, Statistics> computeStatistics(Path hProfFileBefore, Path hProfFileAfter) {
        Map<String, Statistics> statisticsByName = new HashMap<>();
        javaClassFilters.forEach(javaClassFilter -> statisticsByName.put(javaClassFilter.name(), new Statistics()));
        countInstances(hProfFileBefore, statisticsByName, statistics -> statistics.countBefore.incrementAndGet());
        countInstances(hProfFileAfter, statisticsByName, statistics -> statistics.countAfter.incrementAndGet());
        statisticsByName.forEach((name, statistics) -> {
            long countBefore = statistics.countBefore.get();
            long countAfter = statistics.countAfter.get();
            statistics.deltaCount = countAfter - countBefore;
            statistics.deltaPercent = (countBefore == 0) ? 999_999 :
                    100 * ((double) statistics.deltaCount) / countBefore;
        });
        return statisticsByName;
    }

    private void countInstances(Path hProfFile, Map<String, Statistics> instanceStats,
                                Consumer<Statistics> statisticsIncrementer) {
        HeapWrapper heap = hProfFileReader.read(hProfFile);

        stream(heap.getAllInstances().spliterator(), true)
                .forEach(instance ->
                        javaClassFilters.stream()
                                .filter(javaClassFilter -> javaClassFilter.test(instance.getJavaClass()))
                                .forEach(javaClassFilter ->
                                        statisticsIncrementer.accept(instanceStats.get(javaClassFilter.name()))));

    }

    static class Statistics {
        final AtomicLong countBefore = new AtomicLong(0);
        final AtomicLong countAfter = new AtomicLong(0);
        long deltaCount = 0;
        double deltaPercent = 0;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Statistics that = (Statistics) o;
            return deltaCount == that.deltaCount && Double.compare(that.deltaPercent, deltaPercent) == 0
                    && Objects.equals(countBefore.get(), that.countBefore.get())
                    && Objects.equals(countAfter.get(), that.countAfter.get());
        }

        @Override
        public int hashCode() {
            return Objects.hash(countBefore, countAfter, deltaCount, deltaPercent);
        }

        @Override
        public String toString() {
            return "Statistics{" +
                    "countBefore=" + countBefore +
                    ", countAfter=" + countAfter +
                    ", deltaCount=" + deltaCount +
                    ", deltaPercent=" + deltaPercent +
                    '}';
        }
    }
}
