package fr.duminy.memory.analyzer;

public class MemoryAnalyzerException extends RuntimeException {
    public MemoryAnalyzerException(Exception e) {
        super(e);
    }
}
