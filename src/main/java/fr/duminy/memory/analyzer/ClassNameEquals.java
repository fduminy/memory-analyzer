package fr.duminy.memory.analyzer;

import org.netbeans.lib.profiler.heap.JavaClass;

non-sealed class ClassNameEquals implements JavaClassFilter {
    private final String className;

    ClassNameEquals(String className) {
        this.className = className;
    }

    @Override
    public boolean test(JavaClass javaClass) {
        return className.equals(javaClass.getName());
    }

    @Override
    public String name() {
        return className;
    }
}
