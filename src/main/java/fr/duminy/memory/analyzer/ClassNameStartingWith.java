package fr.duminy.memory.analyzer;

import org.netbeans.lib.profiler.heap.JavaClass;

non-sealed class ClassNameStartingWith implements JavaClassFilter {
    private final String beginOfClassName;

    ClassNameStartingWith(String beginOfClassName) {
        this.beginOfClassName = beginOfClassName;
    }

    @Override
    public boolean test(JavaClass javaClass) {
        return javaClass.getName().startsWith(beginOfClassName);
    }

    @Override
    public String name() {
        return beginOfClassName;
    }
}
