package fr.duminy.memory.analyzer;

import org.netbeans.lib.profiler.heap.Heap;
import org.netbeans.lib.profiler.heap.HeapFactory;
import org.netbeans.lib.profiler.heap.Instance;
import org.netbeans.lib.profiler.heap.JavaClass;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

class HProfFileReader {
    HeapWrapper read(Path hProfFile) {
        Heap heap;
        try {
            heap = HeapFactory.createFastHeap(hProfFile.toFile());
        } catch (IOException e) {
            throw new MemoryAnalyzerException(e);
        }
        return new DefaultHeapWrapper(heap);
    }

    public interface HeapWrapper {

        List<JavaClass> getAllClasses();

        Iterable<Instance> getAllInstances();
    }

    private static class DefaultHeapWrapper implements HeapWrapper {
        private final Heap heap;

        private DefaultHeapWrapper(Heap heap) {
            this.heap = heap;
        }

        @Override
        public List<JavaClass> getAllClasses() {
            return heap.getAllClasses();
        }

        @Override
        public Iterable<Instance> getAllInstances() {
            return heap.getAllInstances();
        }
    }
}
