package fr.duminy.memory.analyzer;

import org.netbeans.lib.profiler.heap.JavaClass;

import java.util.function.Predicate;

public sealed interface JavaClassFilter extends Predicate<JavaClass> permits ClassNameEquals, ClassNameStartingWith {
    String name();
}
