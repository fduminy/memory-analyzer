package fr.duminy.memory.analyzer;


import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.IOException;
import java.nio.file.Path;

import static fr.duminy.memory.analyzer.MemoryDumper.HPROF_EXTENSION;

@ExtendWith({SoftAssertionsExtension.class})
class MemoryDumperTest {
    @ParameterizedTest(name = "withHProfExtension={0}")
    @ValueSource(booleans = {false, true})
    void dumpHeap(boolean withHProfExtension, @TempDir Path tempDir, SoftAssertions softly) throws IOException {
        String baseFileName = "dump";
        String expectedFileName = baseFileName + HPROF_EXTENSION;
        if (withHProfExtension) {
            baseFileName += HPROF_EXTENSION;
        }
        Path hProfFile = new MemoryDumper().dumpHeap(tempDir, baseFileName);

        softly.assertThat(hProfFile).exists().isNotEmptyFile();
        softly.assertThat(hProfFile).hasParent(tempDir);
        softly.assertThat(hProfFile).extracting(Path::getFileName).hasToString(expectedFileName);
    }
}