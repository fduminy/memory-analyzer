package fr.duminy.memory.analyzer;

import fr.duminy.memory.analyzer.MemoryDumpAnalyzer.Statistics;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.nio.file.Path;
import java.util.Map;

import static fr.duminy.memory.analyzer.MemoryDumpAnalyzerTest.statistics;
import static java.lang.System.lineSeparator;
import static java.util.Map.of;
import static org.assertj.core.api.Assertions.assertThat;

class StatisticsWriterTest {
    @Test
    void write(@TempDir Path tempDir) {
        StatisticsWriter writer = new StatisticsWriter();
        Map<String, Statistics> statisticsByFilterName = of("class1", statistics(1, 2));
        Path statisticsFile = tempDir.resolve("file.csv");

        writer.write(statisticsByFilterName, statisticsFile);

        assertThat(statisticsFile).hasContent("name,countBefore,countAfter,deltaCount,deltaPercent" + lineSeparator() +
                "class1,\"         1\",\"         2\",\"         1\",\"       100\"");
    }
}