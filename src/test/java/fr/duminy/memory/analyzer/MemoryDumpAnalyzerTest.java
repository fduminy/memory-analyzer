package fr.duminy.memory.analyzer;

import fr.duminy.memory.analyzer.HProfFileReader.HeapWrapper;
import fr.duminy.memory.analyzer.MemoryDumpAnalyzer.Statistics;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.netbeans.lib.profiler.heap.Instance;
import org.netbeans.lib.profiler.heap.JavaClass;

import java.nio.file.Path;
import java.util.Map;

import static java.util.Arrays.stream;
import static java.util.Map.of;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MemoryDumpAnalyzerTest {

    @Mock
    private HProfFileReader hProfFileReader;
    @Mock
    private StatisticsWriter statisticsWriter;

    @Nested
    class WhenAnalyzing {
        @Mock
        private Path hProfFileBefore;
        @Mock
        private Path hProfFileAfter;
        @Mock
        private HeapWrapper heapBefore;
        @Mock
        private HeapWrapper heapAfter;
        @Mock
        private Instance instance1;
        @Mock
        private Instance instance2;
        @Mock
        private JavaClass class1;
        @Mock
        private ClassNameEquals javaClassFilter;

        @SuppressWarnings("unchecked")
        @Test
        void then_write_statistics_file(@TempDir Path tempDir) {
            when(javaClassFilter.name()).thenReturn("class1");
            when(javaClassFilter.test(class1)).thenReturn(true);
            MemoryDumpAnalyzer underTest = new MemoryDumpAnalyzer(hProfFileReader, statisticsWriter, javaClassFilter);
            Path statisticsFile = tempDir.resolve("statistics.csv");
            lenient().when(class1.getName()).thenReturn("class1");
            MockObject object1 = new MockObject(instance1, class1);
            MockObject object2 = new MockObject(instance2, class1);
            mockHeap(heapBefore, hProfFileBefore, object1);
            mockHeap(heapAfter, hProfFileAfter, object1, object2);

            underTest.analyze(hProfFileBefore, hProfFileAfter, statisticsFile);

            ArgumentCaptor<Map<String, Statistics>> statisticsByFilterName = ArgumentCaptor.forClass(Map.class);
            verify(statisticsWriter).write(statisticsByFilterName.capture(), eq(statisticsFile));
            assertThat(statisticsByFilterName.getValue())
                    .containsExactlyInAnyOrderEntriesOf(
                            of("class1", statistics(1, 2))
                    );
        }

        private void mockHeap(HeapWrapper heap, Path hProfFile, MockObject... mockObjects) {
            stream(mockObjects).forEach(mockObject -> when(mockObject.instance().getJavaClass()).thenReturn(mockObject.javaClass()));
            when(heap.getAllInstances()).thenReturn(stream(mockObjects).map(MockObject::instance).collect(toList()));
            when(hProfFileReader.read(hProfFile)).thenReturn(heap);
        }

        private record MockObject(Instance instance, JavaClass javaClass) {
        }
    }

    static Statistics statistics(long countBefore, long countAfter) {
        Statistics statistics = new Statistics();
        statistics.countBefore.set(countBefore);
        statistics.countAfter.set(countAfter);
        statistics.deltaCount = countAfter - countBefore;
        statistics.deltaPercent = 100 * ((double) statistics.deltaCount / countBefore);
        return statistics;
    }
}